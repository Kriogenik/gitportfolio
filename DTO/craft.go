package DTO

import "../models"

// Прием данных для создания анкеты товара
type Craft struct {
	ArrMetroId    []int
	ArrFilterId   []int
	Questionnaire models.Questionnaire
}
