package DTO

import "../models"

// Для приема данных для поиска по фильтрам
type SearchByFilter struct {
	PriceMin int `sign:">="`
	PriceMax int `sign:"<="`
	Filters  []models.Filter
}
