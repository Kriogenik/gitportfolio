package main

import (
	"fmt"

	"./router"

	"./db"
)

func main() {

	e := router.New()

	db.Init()

	fmt.Println("Welcome to the webserver")

	e.Start(":3000")

}
