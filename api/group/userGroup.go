package group

import (
	"../handlers"

	"github.com/labstack/echo"
)

// Здесь происходит роутинг функция пользователя
func UserGroup(g *echo.Group) {

	g.GET("/PersonalArea", handlers.PersonalArea)
	g.GET("/GetOne/:id", handlers.GetOne)

	g.POST("/CreateQuestionnaire", handlers.CreateQuestionnaire)
	g.POST("/UploadPhoto/:id", handlers.UploadPhoto)
	g.POST("/AssociateFilters/:id", handlers.AssociateFilters)

	g.PUT("/ChangeStatus/:id", handlers.ChangeStatus)

	g.DELETE("/Delete/:id", handlers.DeleteQuestionnaire)
	g.DELETE("/DeletePhoto/:id", handlers.DeletePhoto)

}
