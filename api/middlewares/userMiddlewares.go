package middlewares

import (
	"../../models"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func UserMiddlewares(g *echo.Group) {
	g.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		Claims:        &models.JwtCustomClaims{},
		SigningMethod: "HS512",
		SigningKey:    []byte("IceIceBaby"),
	}))
}
