package crypt

import (
	"crypto/sha1"
	"encoding/hex"
)

// хешируем все
func Hashing(login, password string) string {

	//хеширование алгоритмом sha1 c гибридной солью
	saltpswd := []byte(login + "#$%:?<><BYUTDTYVUHYWGUTFDUYa6gdegwycv!@#$%" + password + "my_super_puper_secret_phrase")

	var res [20]byte = sha1.Sum(saltpswd)

	return hex.EncodeToString(res[:])
}
