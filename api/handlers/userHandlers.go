package handlers

import (
	"io"
	"net/http"
	"os"
	"strconv"

	"../../DTO"
	"../../db"
	"../../models"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

////////////////////// Personal Area //////////////////////////

// Заходим в личный кабинет
func PersonalArea(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	user := claims.User

	db := db.Manager()
	db.Preload("Questionnaires.Photos", "Type = ?", models.MainPhoto).First(&user)

	return c.JSON(http.StatusOK, user)
}

////////////////////// Personal Area //////////////////////////

////////////////////// ADD //////////////////////////
// Создаем новую анкету
func CreateQuestionnaire(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	user := claims.User

	craft := DTO.Craft{}
	if err := c.Bind(&craft); err != nil {
		return err
	}

	questionnaire := craft.Questionnaire
	questionnaire.UserID = user.ID

	db := db.Manager()
	db.Create(&questionnaire)

	ArrMetroID := craft.ArrMetroId
	ArrMetro := []models.Metro{}
	db.Where(ArrMetroID).Find(&ArrMetro)
	db.Model(&questionnaire).Association("Metro").Append(ArrMetro)

	ArrFilterId := craft.ArrFilterId
	ArrFilter := []models.Filter{}
	db.Where(ArrFilterId).Find(&ArrFilter)
	db.Model(&questionnaire).Association("Filters").Append(ArrFilter)

	form, err := c.MultipartForm()
	if err != nil {
		return err
	}

	files := form.File["photos"]
	for i, file := range files {
		src, err := file.Open()
		if err != nil {
			return err
		}
		defer src.Close()

		photo := models.Photo{
			Path:            "./assets/photos/" + file.Filename,
			Status:          true,
			QuestionnaireID: questionnaire.ID,
			Type:            models.UsualPhoto,
		}

		if i == 0 {
			photo.Type = models.MainPhoto
		}

		dst, err := os.Create(photo.Path)
		if err != nil {
			return err
		}
		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return err
		}

		db.Create(&photo)
	}

	return c.JSON(http.StatusOK, questionnaire)
}

// Загрузка доп. фоток в анкету
func UploadPhoto(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	user := claims.User

	form, err := c.MultipartForm()
	if err != nil {
		return err
	}

	num, _ := strconv.Atoi(c.Param("id"))
	questionnaireID := uint(num)

	questionnaire := models.Questionnaire{}

	db := db.Manager()
	db.First(&questionnaire, questionnaireID)
	if user.ID != questionnaire.UserID {
		return err
	}

	files := form.File["photos"]
	for _, file := range files {
		src, err := file.Open()
		if err != nil {
			return err
		}
		defer src.Close()

		photo := models.Photo{
			Path:            "./assets/photos/" + file.Filename,
			Status:          true,
			QuestionnaireID: questionnaireID,
			Type:            models.UsualPhoto,
		}

		dst, err := os.Create(photo.Path)
		if err != nil {
			return err
		}
		defer dst.Close()

		if _, err = io.Copy(dst, src); err != nil {
			return err
		}

		db.Create(&photo)
	}

	return c.JSON(http.StatusOK, files)
}

////////////////////// ADD //////////////////////////

////////////////////// GET //////////////////////////
// Открыть конкретную анкету товара (Ну, для добавки фотки или же редактирования)
func GetOne(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	user := claims.User

	questionnaire := models.Questionnaire{}
	id := c.Param("id")

	db := db.Manager()
	db.Preload("Photos").First(&questionnaire, id)

	if user.ID != questionnaire.UserID {
		return c.String(http.StatusOK, "Ошибочку надо будет написать, на случай говнеца")
	}

	return c.JSON(http.StatusOK, questionnaire)
}

////////////////////// GET //////////////////////////

////////////////////// DELETE //////////////////////////
// Удаление анкеты товара
func DeleteQuestionnaire(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	user := claims.User

	db := db.Manager()

	questionnaire := models.Questionnaire{}

	id := c.Param("id")

	db.First(&questionnaire, id)

	if user.ID != questionnaire.UserID {
		return c.String(http.StatusOK, "Ошибочку надо будет написать, на случай говнеца")
	}

	db.Delete(&questionnaire)

	return c.JSON(http.StatusOK, questionnaire)
}

// Удалить конкретную фотографию
func DeletePhoto(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	user := claims.User

	photo := models.Photo{}
	questionnaire := models.Questionnaire{}

	num, _ := strconv.Atoi(c.Param("id"))
	photoid := uint(num)

	db := db.Manager()
	db.First(&photo, photoid)
	db.First(&questionnaire, photo.QuestionnaireID)

	if user.ID != questionnaire.UserID {
		return c.String(http.StatusOK, "Ошибочку надо будет написать, на случай говнеца")
	}

	db.Delete(&photo)

	return c.JSON(http.StatusOK, photo)
}

////////////////////// DELETE //////////////////////////

////////////////////// UPDATE //////////////////////////
// Редактировать нужную анкету
func UpdateQuestionnaire(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	user := claims.User

	id := c.Param("id")

	questionnaire := models.Questionnaire{}

	db := db.Manager()
	db.First(&questionnaire, id)

	if user.ID != questionnaire.UserID {
		return c.String(http.StatusOK, "Ошибочку надо будет написать, на случай говнеца")
	}

	if err := c.Bind(&questionnaire); err != nil {
		return err
	}

	questionnaire.Moderation = false

	db.Save(&questionnaire)

	return c.JSON(http.StatusOK, questionnaire)
}

// Изменение "тегов" товара, добавка новых, удаление старых
func AssociateFilters(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	user := claims.User

	db := db.Manager()

	questionnaire := models.Questionnaire{}
	FilterArr := []models.Filter{}

	questionnaireID := c.Param("id")
	db.First(&questionnaire, questionnaireID)

	if user.ID != questionnaire.UserID {
		return c.String(http.StatusOK, "Ошибочку надо будет написать, на случай говнеца")
	}

	if err := c.Bind(&FilterArr); err != nil {
		return err
	}

	db.Model(&questionnaire).Association("Filters").Replace(FilterArr)

	return c.JSON(http.StatusOK, questionnaire)
}

// Активация/Деактивация анкеты товара
func ChangeStatus(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(*models.JwtCustomClaims)
	user := claims.User

	db := db.Manager()

	questionnaire := models.Questionnaire{}

	id := c.Param("id")

	db.First(&questionnaire, id)

	if user.ID != questionnaire.UserID {
		return c.String(http.StatusOK, "Ошибочку надо будет написать, на случай говнеца")
	}

	questionnaire.Status = !questionnaire.Status

	db.Save(&questionnaire)

	return c.JSON(http.StatusOK, questionnaire)

}

////////////////////// UPDATE //////////////////////////
