package handlers

import (
	"log"
	"net/http"
	"reflect"
	"strconv"

	"../../DTO"
	"../../db"
	"../../models"
	"../crypt"

	"github.com/labstack/echo"
)

// Регистрируемся
func Registration(c echo.Context) error {
	db := db.Manager()

	user := models.User{}

	if err := c.Bind(&user); err != nil {
		return err
	}

	//хешируем пароль
	user.Password = crypt.Hashing(user.Login, user.Password)

	user.Status = true

	db.Create(&user)

	return c.JSON(http.StatusCreated, user)
}

// логинимся
func Login(c echo.Context) error {
	login := c.FormValue("username")
	password := c.FormValue("password")
	//хеширование пароля
	password = crypt.Hashing(login, password)

	db := db.Manager()

	user := models.User{}

	db.Where(&models.User{Login: login}).First(&user)

	if user.Password == password {
		token, err := crypt.CreateJwtToken(user)
		if err != nil {
			log.Println("Error Creating JWT token", err)
			return c.String(http.StatusInternalServerError, "something went wrong")
		}

		return c.JSON(http.StatusOK, echo.Map{
			"token": token,
		})
	}

	return c.String(http.StatusUnauthorized, "Your username or password were wrong")
}

// функция для отправки на клиент ВСЕХ активированных анкет прошедших модерацию
func GetAllQuestionnaire(c echo.Context) error {

	questionnaires := []models.Questionnaire{}

	db := db.Manager()
	db.Where(&models.Questionnaire{Status: true, Moderation: true}).Preload("Photos", "Type = ?", models.MainPhoto).Find(&questionnaires)

	return c.JSON(http.StatusOK, questionnaires)
}

func GetOneQuestionnaire(c echo.Context) error {
	questionnaire := models.Questionnaire{}

	id := c.Param("id")
	db := db.Manager()
	db.Preload("Photos", "Status = ?", "true").First(&questionnaire, id)

	return c.JSON(http.StatusOK, questionnaire)
}

// Принимаем фильтры для поиска на главной странице
func Filters(c echo.Context) error {
	// Создаю объект фильтра и заполняю его данными
	filter := DTO.SearchByFilter{}
	if err := c.Bind(&filter); err != nil {
		return err
	}

	// Получаю информацию о типе и значения свойств в виде коллекции
	t := reflect.TypeOf(filter)
	v := reflect.ValueOf(filter)

	where := "Status = true AND Moderation = true "

	// костыль
	check := 0

	// Пробегаюсь по каждому свойству
	for i := 0; i < v.NumField(); i++ {
		// Получаю название свойства
		name := t.Field(i).Name

		if t.Field(i).Type != reflect.TypeOf(check) {
			continue
		}
		// Получаю тег `sign`, который хранится в метаданных свойства
		sign := t.Field(i).Tag.Get("sign")
		// Получаю значение свойства
		// Конвертирую свойство в interface -> int -> string
		val := strconv.Itoa(v.Field(i).Interface().(int))

		// Добавы
		if val != "0" {
			where += " AND " + name + " " + sign + " " + val
		}
	}

	questionnaires := []models.Questionnaire{}
	db := db.Manager()
	db.Where(where).Where(filter.Filters).Preload("Photos", "Type = ?", models.MainPhoto).Preload("Filters").Find(&questionnaires)

	return c.JSON(http.StatusOK, questionnaires)
}
