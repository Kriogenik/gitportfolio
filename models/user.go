package models

import "github.com/jinzhu/gorm"

type User struct {
	gorm.Model
	Login          string
	Password       string
	Email          string
	Metro          string
	Cash           int
	Status         bool
	Questionnaires []Questionnaire `gorm:"FOREIGNKEY:UserID"`
}
