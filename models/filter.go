package models

import "github.com/jinzhu/gorm"

// Теги, фильтры для товаров
type Filter struct {
	gorm.Model
	Group          string
	Name           string
	Questionnaires []Questionnaire `gorm:"many2many:questionnaire_filter;"`
	Status         bool
}
