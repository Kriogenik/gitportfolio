package models

import (
	"github.com/jinzhu/gorm"
)

type Questionnaire struct {
	gorm.Model
	UserID      uint
	Price       int
	Phone       string
	Name        string
	Description string
	Metro       []Metro  `gorm:"many2many:questionnaire_metro;"`
	Filters     []Filter `gorm:"many2many:questionnaire_filter;"`
	Photos      []Photo  `gorm:"FOREIGNKEY:QuestionnaireID"`
	Moderation  bool
	Status      bool
}
