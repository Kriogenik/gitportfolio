package router

import (
	"../api/group"
	"../api/handlers"
	"../api/middlewares"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

// МААААААХИИИИЯЯ
func New() *echo.Echo {
	e := echo.New()

	//читабельные логи
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "[${time_rfc3339}] ${status} ${method} ${host}${uri} ${error}\n",
	}))

	e.Use(middleware.Recover())

	////////////////////// USERS //////////////////////////
	// Группируем функции
	// Подрубаем мидл
	usersGroup := e.Group("/user")
	middlewares.UserMiddlewares(usersGroup)
	group.UserGroup(usersGroup)
	////////////////////// USERS //////////////////////////

	////////////////////// MAIN ///////////////////////////
	e.POST("/registration", handlers.Registration)
	e.POST("/login", handlers.Login)
	e.GET("/filters", handlers.Filters)
	////////////////////// MAIN ///////////////////////////
	return e

}
