package db

import (
	"../models"

	"github.com/jinzhu/gorm"

	_ "github.com/go-sql-driver/mysql"
)

// Создаем переменную для работы с ДБ
var db *gorm.DB

// Запускаем инициализацию ДБ, создаем соединения с ДБ
// Запускаем миграцию ДБ, то есть она создает нужную нам структуру в таблице
func Init() {
	connect()
	migrate()
	db.LogMode(true)
}

// Создаем функцию для соединения с ДБ
func connect() {
	// Данные для доступа к ДБ
	DBMS := "mysql"
	USER := "root"
	PASS := "100496"
	PROTOCOL := "tcp(0.0.0.0:3306)"
	DBNAME := "whoreDB"
	PARAMS := "?parseTime=true"
	// Связываем все элементы для входа
	CONNECT := USER + ":" + PASS + "@" + PROTOCOL + "/" + DBNAME + PARAMS
	// Создаем ошибку
	var err error
	// Подключаем в db открытие соединения с ДБ
	db, err = gorm.Open(DBMS, CONNECT)
	// Создаем поведение на случай ошибки
	if err != nil {
		panic(err.Error())
	}
}

// Создаем функцию отвечающую за создание нужных таблиц
func migrate() {
	db.AutoMigrate(&models.User{}, &models.Filter{}, &models.Questionnaire{}, &models.Photo{})
}

// Менеджер для подключения к соединению ДБ
func Manager() *gorm.DB {
	return db
}
